package com.example.thind.gestures;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;

/**
 * Created by Thind on 12-10-2015.
 */
public class Adapter extends BaseAdapter {
    int numRows, numCols, width, height;
    Context context;

    public Adapter(Context context, int numRows, int numCols, float height, float width) {
        this.context = context;
        this.numRows = numRows;
        this.numCols = numCols;
        this.height = (int) (height / numRows);
        this.width = (int) (width / numCols);
    }

    @Override
    public int getCount() {
        return numRows * numCols;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (height < width)
            width = height;
        else if (width < height)
            height = width;

        AbsListView.LayoutParams params = new AbsListView.LayoutParams(width, height);
        LayoutInflater li = LayoutInflater.from(context);
        convertView = li.inflate(R.layout.grid_column, parent, false);
        convertView.setLayoutParams(params);
        return convertView;
    }
}
