package com.example.thind.gestures;

import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Activity2 extends AppCompatActivity {
    RelativeLayout relativeLayout2;
    GradientDrawable GD1, GD2, GD3, GD4, GD5, GD6;
    View circle1, circle2, circle3, circle4, circle5, circle6;
    String s_circle1 = "Inside circle 1";
    String s_circle2 = "Inside circle 2";
    String s_circle3 = "Inside circle 3";
    String s_circle4 = "Inside circle 4";
    String s_circle5 = "Inside circle 5";
    String s_circle6 = "Inside circle 6";
    TextView tv2;
    float centerX, centerY, a, b, x, y;
    Button click;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        declarations();
        click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent act3 = new Intent(Activity2.this, Activity3.class);
                startActivity(act3);
            }
        });
        relativeLayout2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                x = event.getX();
                y = event.getY();

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        action(circle1, GD1, s_circle1);
                        action(circle2, GD2, s_circle2);
                        action(circle3, GD3, s_circle3);
                        action(circle4, GD4, s_circle4);
                        action(circle5, GD5, s_circle5);
                        action(circle6, GD6, s_circle6);
                        break;
                    case MotionEvent.ACTION_MOVE:
                        action(circle1, GD1, s_circle1);
                        action(circle2, GD2, s_circle2);
                        action(circle3, GD3, s_circle3);
                        action(circle4, GD4, s_circle4);
                        action(circle5, GD5, s_circle5);
                        action(circle6, GD6, s_circle6);
                        break;
                }
                return true;
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected float centerofY(View circle) {
        centerY = (circle.getTop() + circle.getBottom()) / 2;
        return centerY;
    }

    protected float centerofX(View cir) {
        centerX = (cir.getLeft() + cir.getRight()) / 2;
        return centerX;
    }

    protected float radius(View view) {
        float radius;
        a = view.getLeft();
        b = view.getRight();
        radius = (b - a) / 2;
        return radius;
    }

    protected float distance(float x, float y, View circ) {
        float distance = (float) Math.sqrt(Math.pow(centerofX(circ) - x, 2) + (Math.pow(centerofY(circ) - y, 2)));
        return distance;
    }

    protected void action(View circle, GradientDrawable gradient, String text) {
        if (distance(x, y, circle) < radius(circle)) {
            tv2.setText(text);
        } else if (distance(x, y, circle) == radius(circle)) {
            tv2.setText("On The Boundary");
        } else {
            tv2.setText("Outside the circle");
//            gradient.setColor(Color.RED);
        }
    }
//
//    protected void actionmove(View circle, GradientDrawable gradient, String text) {
//        if (distance(x, y, circle) < radius(circle)) {
//            tv2.setText(text);
//            gradient.setColor(Color.GREEN);
//        } else if (distance(x, y, circle) == radius(circle)) {
//            tv2.setText("On The Boundary");
//        } else {
//            tv2.setText("Outside the circle");
//            gradient.setColor(Color.RED);    /*cancel it for phase 2 */
//        }
//    }

    protected void declarations() {
        circle1 = (View) findViewById(R.id.circle1);
        circle2 = (View) findViewById(R.id.circle2);
        circle3 = (View) findViewById(R.id.circle3);
        circle4 = (View) findViewById(R.id.circle4);
        circle5 = (View) findViewById(R.id.circle5);
        circle6 = (View) findViewById(R.id.circle6);
        GD1 = (GradientDrawable) circle1.getBackground();
        GD2 = (GradientDrawable) circle2.getBackground();
        GD3 = (GradientDrawable) circle3.getBackground();
        GD4 = (GradientDrawable) circle4.getBackground();
        GD5 = (GradientDrawable) circle5.getBackground();
        GD6 = (GradientDrawable) circle6.getBackground();
        tv2 = (TextView) findViewById(R.id.tv2);
        relativeLayout2 = (RelativeLayout) findViewById(R.id.relativelayout2);
        click = (Button) findViewById(R.id.click1);
    }
}

