package com.example.thind.gestures;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;

public class Activity3 extends AppCompatActivity implements View.OnTouchListener, View.OnClickListener {

    GridView gridView;
    GradientDrawable bgShape;
    Adapter gridViewAdapter;
    View viewCircle;
    float radius;
    float xCenter;
    float yCenter;
    TextView onTouchStatus;
    Button btNextLevel;
    int numRows, numCols;
    EditText etRows, etCols;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_3);
        init();
        setOnClick();
    }

    public void init() {
        gridView = (GridView) findViewById(R.id.grid);
        onTouchStatus = (TextView) findViewById(R.id.tv2);
        etRows = (EditText) findViewById(R.id.rows);
        etCols = (EditText) findViewById(R.id.cols);
        btNextLevel = (Button) findViewById(R.id.Enter);

    }

    public void setOnClick() {
        btNextLevel.setOnClickListener(Activity3.this);
    }

    public void hideKeybord() {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isActive())
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        float currentXPosition = event.getX();
        float currentYPosition = event.getY();
        int position = gridView.pointToPosition((int) currentXPosition, (int) currentYPosition);
        if (position < 0) {
            onTouchStatus.setText("Outside The Circle");
            return false;
        }
        setCircleInfo(position);
        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                process(event.getX(), event.getY(),position);
                break;
            case MotionEvent.ACTION_DOWN:
                process(event.getX(), event.getY(),position);

                break;
            case MotionEvent.ACTION_MOVE:
                process(event.getX(), event.getY(),position);

                break;
        }

        return true;
    }

    public void process(float currX, float currY,int position) {
        double dist;
        dist = getDistance(currX, currY);
        if (dist < radius) {
            toggleColor(1,position);
        } else if (dist == radius) {
            toggleColor(2,position);
        } else {
            toggleColor(3,position);
        }
    }

    public void setCircleInfo(int pos) {
        viewCircle = gridView.getChildAt(pos);
        bgShape = (GradientDrawable) viewCircle.getBackground();
        radius = (viewCircle.getWidth()) / 2;

        xCenter = (viewCircle.getRight() + viewCircle.getLeft()) / 2;
        yCenter = (viewCircle.getBottom() + viewCircle.getTop()) / 2;
    }

    public double getDistance(float xPos, float yPos) {
        double distance;
        distance = Math.sqrt(Math.pow((xPos - xCenter), 2) + Math.pow((yPos - yCenter), 2));
        return distance;
    }

    public void toggleColor(int pos,int position) {
        switch (pos) {
            case 1:
                bgShape.setColor(Color.BLUE);
                onTouchStatus.setText("Inside The Circle "+position);
                break;
            case 3:
                bgShape.setColor(Color.BLUE);
                bgShape.setColor(Color.BLACK);
                onTouchStatus.setText("Outside The Circle");
                break;
        }
    }

    @Override
    public void onClick(View v) {
        hideKeybord();
        switch (v.getId()) {
            case R.id.Enter:
                numRows = Integer.parseInt(etRows.getText().toString());
                numCols = Integer.parseInt(etCols.getText().toString());
                if ((numRows > 0 && numRows < 5) && (numCols > 0 && numCols < 5)) {
                    onTouchStatus.setTextColor(Color.BLACK);
                    onTouchStatus.setText("");
                    float gridViewHeight = gridView.getHeight(), gridViewWidth = gridView.getWidth();
                    gridView.setNumColumns(numCols);
                    gridViewAdapter = new Adapter(Activity3.this, numRows, numCols, gridViewHeight, gridViewWidth);
                    gridView.setAdapter(gridViewAdapter);
                    gridViewAdapter.notifyDataSetChanged();
                    gridView.setOnTouchListener(Activity3.this);
                } else {
                    onTouchStatus.setTextColor(Color.RED);
                    onTouchStatus.setText("Enter Correct Values");
                }
                break;
        }
    }

}